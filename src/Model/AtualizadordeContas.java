/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

/**
 *
 * @author Ricardo
 */
public class AtualizadordeContas {
    private double saldoTotal = 0;
    private double selic;
       
    public AtualizadordeContas(double selic) {
    this.selic = selic;
    }
    public void roda(Conta c) {
        System.out.println(c.getSaldo());
        c.atualiza(selic);
        this.saldoTotal+=c.getSaldo();
        System.out.println(c.getSaldo());
    }

    public double getSaldoTotal() {
        return saldoTotal;
    }
    
}
